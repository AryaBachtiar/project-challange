package com.example.project.service;

import com.example.project.model.Challange;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public interface ChallangeService {


    Object getAllChallange();

    Map<String, Object> login(Challange challange);

    Challange addChallange(Challange challange);

    Challange getChallangeById(Long id);

    Challange editChallangeById(Long id, String email, String password, String username);

    void deleteChallangeById(Long id);
}