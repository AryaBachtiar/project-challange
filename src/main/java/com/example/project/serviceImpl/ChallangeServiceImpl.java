package com.example.project.serviceImpl;

import com.example.project.Repository.ChallangeRepository;
import com.example.project.model.Challange;
import com.example.project.service.ChallangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ChallangeServiceImpl implements ChallangeService {
    //Dan di sini untuk repository sebelum nya di panggil agar bisa berfungsi
    @Autowired
    private ChallangeRepository challangeRepository;


    @Override
    public  Map<String, Object> login(Challange registers) {
        Challange user = challangeRepository.findByPassword(registers.getPassword()).get();
        Challange users = challangeRepository.findByEmail(registers.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("user", users);
        response.put("user" , user);
        return response;
    }



    // Overide Seluruh Nya Agar Bisa masuk ke dalam Data Header

    @Override
    public Challange addChallange(Challange challange) {
        Challange tambah = new Challange();
        tambah.setEmail(challange.getEmail());
        tambah.setPassword(challange.getPassword());
        tambah.setUsername(challange.getUsername());
        return challangeRepository.save(tambah);
    }


    @Override
    public Challange getChallangeById(Long id) {
        return challangeRepository.findById(id).get();
    }

    //    Semua Colum yang di tambahkan Agarv terpanggil yang di dalam package model
    @Override
    public Challange editChallangeById(Long id, String email, String password, String username) {
        Challange challange = challangeRepository.findById(id).get();
        challange.setEmail(email);
        challange.setPassword(password);
        challange.setUsername(username);
        return challangeRepository.save(challange);
    }

    @Override
    public List<Challange> getAllChallange() {
        return challangeRepository.findAll();
    }

    @Override
    public void deleteChallangeById(Long id) {
        challangeRepository.deleteById(id);
    }

}
