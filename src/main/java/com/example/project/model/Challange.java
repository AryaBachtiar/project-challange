package com.example.project.model;

import javax.persistence.*;

@Entity
@Table(name = "challange")
public class Challange {
    //    Untuk id Tersebut Akan Membuat Auto Increment/otomatis terisi
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //    Column Untuk Menambahkan Header Data/Data yang di inginkan
    @Column(name = "email")
    private String email;

    //    Column Untuk Menambahkan Header Data/Data yang di inginkan
    @Column(name = "password")
    private String password;

    //     Column Untuk Menambahkan Header Data/Data yang di inginkan
    @Column(name = "username")
    private String username;


    //    Di Sini Untuk id menggunakan constructor
    public Challange() {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}