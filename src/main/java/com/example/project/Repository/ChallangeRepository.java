package com.example.project.Repository;

import com.example.project.model.Challange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ChallangeRepository extends JpaRepository<Challange,Long> {
    Optional<Challange> findByEmail(String email);

    Optional<Challange> findByPassword(String password);
}