package com.example.project.controller;

import com.example.project.model.Challange;
import com.example.project.service.ChallangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/challange")
@CrossOrigin(origins = "http://localhost:3000")
public class ChallangeController {

    //    Autowired Memanggil file dalam pakcage
    @Autowired
    private ChallangeService challangeService;

    @PostMapping("/login")
    public Map<String, Object> login(@RequestBody Challange challange) {
        return challangeService.login(challange);
    }

    //    PostMapping Menambahkan Data
    @PostMapping("/register")
    public Challange addChallange(@RequestBody Challange challange) {
        return challangeService.addChallange(challange);
    }

    //    GetMapping Menampilkan data yang telah di tambahkan sebelumnya sesuai id
    @GetMapping("/{id}")
    public Challange getChallangeById(@PathVariable("id") Long id) {
        return challangeService.getChallangeById(id);
    }

    //    PutMapping Edit Data Yang di pilih Sesuai id
    @PutMapping("/{id}")
    public Challange editChallangeById(@PathVariable("id") Long id, @RequestBody Challange challange) {
        return challangeService.editChallangeById(id, challange.getEmail(), challange.getPassword(), challange.getUsername());
    }

    //    fungsi ini sama yang membedakan adalah menampilkan semua data yang di tambahkan sesuai dengan pemanggilan
    @GetMapping("/all")
    public List<Challange> getAllChallange() {
        return (List<Challange>) challangeService.getAllChallange();
    }

    //    DElete Mapping Untuk Menghapus data sesuai id
    @DeleteMapping("/{id}")
    public void deletechallangeById(@PathVariable("id") Long id) {
        challangeService.deleteChallangeById(id);
    }
}